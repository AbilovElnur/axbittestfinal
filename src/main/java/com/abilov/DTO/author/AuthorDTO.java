package com.abilov.DTO.author;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthorDTO {
    private String firstName;
    private String lastName;
    private String patronymicName;
    private String birthdayDate;
}
