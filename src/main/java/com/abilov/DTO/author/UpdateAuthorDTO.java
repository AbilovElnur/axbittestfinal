package com.abilov.DTO.author;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateAuthorDTO {
    private Long authorId;
    private AuthorDTO author;
}
