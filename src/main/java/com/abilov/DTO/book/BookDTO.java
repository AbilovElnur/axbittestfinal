package com.abilov.DTO.book;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookDTO {
    private Long authorId;
    private Long genreId;
    private Integer isbn;
}
