package com.abilov.DTO.book;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateBookDTO {
    private Long id;
    private BookDTO book;
}
