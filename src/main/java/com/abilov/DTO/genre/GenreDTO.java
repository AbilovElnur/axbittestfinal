package com.abilov.DTO.genre;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenreDTO {
    private String name;
}
