package com.abilov.DTO.genre;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateGenreDTO {
    private Long id;
    private GenreDTO genre;
}
