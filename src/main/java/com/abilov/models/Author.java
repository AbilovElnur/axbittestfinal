package com.abilov.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity(name="Author")
@Table(name="author")
@Data
@Where(clause = "deleted = false")
public class Author extends BaseEntity {
    @Where(clause = "deleted = false")
    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Book> books = new ArrayList<>();

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String patronymicName;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column
    private LocalDate birthdayDate;
}
