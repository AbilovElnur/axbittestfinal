package com.abilov.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
@Data
@Where(clause = "deleted = false")
public class Genre extends BaseEntity {
    @Column(name = "name")
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "genre", fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private List<Book> books = new ArrayList<>();
}
