package com.abilov.repository;

import com.abilov.models.Author;
import com.abilov.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

public interface AuthorRepository extends JpaRepository<Author, Long> {
    @Query("update Author a set a.deleted = true where a.id=?1")
    @Modifying
    public void softDelete(Long id);
}
