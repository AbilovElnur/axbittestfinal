package com.abilov.repository;

import com.abilov.models.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface GenreRepository extends JpaRepository<Genre, Long> {
    @Query("update Genre g set g.deleted = true where g.id=?1")
    @Modifying
    public void softDelete(Long id);
}
