package com.abilov.rest;

import com.abilov.DTO.author.AuthorDTO;
import com.abilov.DTO.author.UpdateAuthorDTO;
import com.abilov.models.Author;
import com.abilov.service.AuthorService;
import com.abilov.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/author")
public class AuthorRestController {

    private final AuthorService authorService;
    private final BookService bookService;


    public AuthorRestController(AuthorService authorService, BookService bookService) {
        this.authorService = authorService;
        this.bookService = bookService;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity<Author> getAuthor(@PathVariable("id") Long authorId) {
        if (authorId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Author customer = this.authorService.getById(authorId);

        if (customer == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    //добавление нового автора
    //authorDTO - дто с данными

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<AuthorDTO> saveAuthor(@RequestBody @Valid AuthorDTO authorDTO) {
        if (authorDTO == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        this.authorService.save(authorDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    //обновление автора
     //дто с идентифтактором автора, которого надо изменить и данными для изменения.

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<UpdateAuthorDTO> updateAuthor(@RequestBody @Valid UpdateAuthorDTO author) {
        this.authorService.updateAuthor(author);
        return new ResponseEntity<>(author, HttpStatus.OK);
    }


    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Author> deleteAuthor(@PathVariable("id") Long id) {
        authorService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //получение всех авторов
    @RequestMapping(value = "getAll", method = RequestMethod.GET)
    public ResponseEntity<List<Author>> getAllAuthors() {
        List<Author> authorList = this.authorService.getAll();
        if (authorList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(authorList, HttpStatus.OK);
    }
}
