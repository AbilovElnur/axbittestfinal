package com.abilov.rest;

import com.abilov.DTO.book.BookDTO;
import com.abilov.DTO.book.UpdateBookDTO;
import com.abilov.models.Book;
import com.abilov.repository.BookRepository;
import com.abilov.service.BookService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/book")
public class BookRestController {
    private final BookService bookService;
    private final BookRepository bookRepository;

    public BookRestController(BookService bookService, BookRepository bookRepository) {
        this.bookService = bookService;
        this.bookRepository = bookRepository;
    }

    //добавление книги
    //bookDTO - дто с данными

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Book> updateBook(@RequestBody @Valid BookDTO bookDTO) {
        return new ResponseEntity<>(bookService.save(bookDTO), HttpStatus.OK);
    }

    //изменение книги
    //updateBookDTO - дто с идентифтактором книги, которую надо изменить и данными для изменения.

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<Book> updateBook(@RequestBody @Valid UpdateBookDTO updateBookDTO) {
        return new ResponseEntity<>(bookService.update(updateBookDTO), HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteBook(@PathVariable("id") Long id) {
        if (bookService.getById(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        bookService.deleteById(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/getAll")
    public ResponseEntity<List<Book>> getAllBooks() {
        return new ResponseEntity<>(bookService.getAll(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "pageable")
    public List<Book> findPaginated(final Pageable pageable) {
        Page<Book> resultPage = bookRepository.findAll(pageable);
        return resultPage.getContent();
    }
}
