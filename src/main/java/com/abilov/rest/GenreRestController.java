package com.abilov.rest;

import com.abilov.DTO.genre.GenreDTO;
import com.abilov.DTO.genre.UpdateGenreDTO;
import com.abilov.models.Genre;
import com.abilov.service.GenreService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/genre")
public class GenreRestController {
    private final GenreService genreService;

    public GenreRestController(GenreService genreService) {
        this.genreService = genreService;
    }

    //добавление жанра
    //genreDTO дто с данными

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Genre> addGenre(@RequestBody GenreDTO genreDTO) {
        return new ResponseEntity<>(genreService.save(genreDTO), HttpStatus.OK);
    }

    //удаление жанра
    //id - идентификатор жанра

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteGenre(@PathVariable("id") Long id) {
        genreService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

     //обновление жанра
     //updateGenreDTO дто с идентификатором жанра и данными для обновления (по сути только имя)

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<Genre> updateGenre(@RequestBody UpdateGenreDTO updateGenreDTO) {
        return new ResponseEntity<>(genreService.update(updateGenreDTO), HttpStatus.OK);
    }

}
