package com.abilov.service;

import com.abilov.DTO.author.AuthorDTO;
import com.abilov.DTO.author.UpdateAuthorDTO;
import com.abilov.models.Author;

import java.util.List;

public interface AuthorService {

    Author getById(Long id);

    void updateAuthor(UpdateAuthorDTO updateAuthorDTO);

    void save(AuthorDTO authorDTO);

    void deleteById(Long id);

    List<Author> getAll();
}
