package com.abilov.service;

import com.abilov.DTO.author.AuthorDTO;
import com.abilov.DTO.author.UpdateAuthorDTO;
import com.abilov.models.Author;
import com.abilov.repository.AuthorRepository;
import com.abilov.repository.BookRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class AuthorServiceImpl implements AuthorService {
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;

    public AuthorServiceImpl(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public Author getById(Long id) {
        return authorRepository.findById(id).orElseThrow(() -> new RuntimeException("author not found"));
    }

    @Override
    public void save(AuthorDTO authorDTO) {
        Author author = new Author();
        author.setFirstName(authorDTO.getFirstName());
        author.setLastName(authorDTO.getLastName());
        author.setPatronymicName(authorDTO.getPatronymicName());
        author.setBirthdayDate(LocalDate.parse(authorDTO.getBirthdayDate()));
        authorRepository.save(author);
    }

    @Override
    public void updateAuthor(UpdateAuthorDTO updateAuthorDTO) {
        Author author = this.getById(updateAuthorDTO.getAuthorId());
        author.setFirstName(updateAuthorDTO.getAuthor().getFirstName());
        author.setLastName(updateAuthorDTO.getAuthor().getLastName());
        author.setBirthdayDate(LocalDate.parse(updateAuthorDTO.getAuthor().getBirthdayDate()));
        author.setPatronymicName(updateAuthorDTO.getAuthor().getPatronymicName());
        authorRepository.save(author);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        Author author = authorRepository.findById(id).orElseThrow(() -> new RuntimeException("author not found"));
        // мягко удаляем книги
        author.getBooks().forEach(b -> bookRepository.softDelete(b.getId()));
        // мяго удаляем автора
        authorRepository.softDelete(id);
    }

    @Override
    public List<Author> getAll() {
        return authorRepository.findAll();
    }
}
