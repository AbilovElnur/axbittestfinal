package com.abilov.service;

import com.abilov.DTO.book.BookDTO;
import com.abilov.DTO.book.UpdateBookDTO;
import com.abilov.models.Book;

import java.util.List;

public interface BookService {

    Book getById(Long id);

    Book save(BookDTO bookDTO);

    Book update(UpdateBookDTO updateBookDTO);

    void deleteById(Long id);

    List<Book> getAll();
}
