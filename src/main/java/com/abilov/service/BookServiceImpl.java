package com.abilov.service;

import com.abilov.DTO.book.BookDTO;
import com.abilov.DTO.book.UpdateBookDTO;
import com.abilov.models.Book;
import com.abilov.repository.BookRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final AuthorService authorService;
    private final GenreService genreService;

    public BookServiceImpl(BookRepository bookRepository, AuthorService authorService, GenreService genreService) {
        this.bookRepository = bookRepository;
        this.authorService = authorService;
        this.genreService = genreService;
    }

    @Override
    public Book getById(Long id) {
        return bookRepository.findById(id).get();
    }

    @Override
    public Book save(BookDTO bookDTO) {
        Book book = new Book();
        book.setAuthor(authorService.getById(bookDTO.getAuthorId()));
        book.setGenre(genreService.getById(bookDTO.getGenreId()));
        book.setISBN(bookDTO.getIsbn());
        return bookRepository.save(book);
    }

    @Override
    public Book update(UpdateBookDTO updateBookDTO) {
        Book book = bookRepository.findById(updateBookDTO.getId()).orElseThrow(() -> new RuntimeException("book not found"));
        book.setAuthor(authorService.getById(updateBookDTO.getBook().getAuthorId()));
        book.setGenre(genreService.getById(updateBookDTO.getBook().getGenreId()));
        book.setISBN(updateBookDTO.getBook().getIsbn());
        return bookRepository.save(book);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        bookRepository.softDelete(id);
    }

    @Override
    public List<Book> getAll() {
        return bookRepository.findAll();
    }
}
