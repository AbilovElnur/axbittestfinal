package com.abilov.service;

import com.abilov.DTO.genre.GenreDTO;
import com.abilov.DTO.genre.UpdateGenreDTO;
import com.abilov.models.Genre;

import java.util.List;

public interface GenreService {

    Genre getById(Long id);

    Genre save(GenreDTO genreDTO);

    Genre update(UpdateGenreDTO updateGenreDTO);

    void deleteById(Long id);

    List<Genre> getAll();

}
