package com.abilov.service;

import com.abilov.DTO.genre.GenreDTO;
import com.abilov.DTO.genre.UpdateGenreDTO;
import com.abilov.models.Author;
import com.abilov.models.Book;
import com.abilov.models.Genre;
import com.abilov.repository.BookRepository;
import com.abilov.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {

    private final GenreRepository genreRepository;
    private final BookRepository bookRepository;

    public GenreServiceImpl(GenreRepository genreRepository, BookRepository bookRepository) {
        this.genreRepository = genreRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public Genre getById(Long id) {
        return genreRepository.findById(id).get();
    }

    @Override
    public Genre save(GenreDTO genreDTO) {
        Genre genre = new Genre();
        genre.setName(genreDTO.getName());
        return genreRepository.save(genre);
    }

    @Override
    public Genre update(UpdateGenreDTO updateGenreDTO) {
        Genre genre = genreRepository.findById(updateGenreDTO.getId()).orElseThrow(() -> new RuntimeException("genre not found"));
        genre.setName(updateGenreDTO.getGenre().getName());
        return genreRepository.save(genre);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        Genre author = genreRepository.findById(id).orElseThrow(() -> new RuntimeException("genre not found"));
        author.getBooks().forEach(b -> bookRepository.softDelete(b.getId()));
        genreRepository.softDelete(id);
    }

    @Override
    public List<Genre> getAll() {
        return genreRepository.findAll();
    }
}
