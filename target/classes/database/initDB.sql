--liquibase formatted sql
--changeset elnur:1
create sequence if not exists hibernate_sequence start with 1 increment by 1;
Create TABLE Author
(
    id              bigserial PRIMARY KEY,
    first_name      VARCHAR(100) NOT NULL,
    last_name       VARCHAR(100) NOT NULL,
    patronymic_name VARCHAR(100) NOT NULL,
    birthday_date   timestamp    NOT NULL
);

Create TABLE Genre
(
    id   bigserial PRIMARY KEY,
    name VARCHAR(100) NOT NULL

);

Create TABLE Book
(
    id        bigserial PRIMARY KEY,
    author_id bigint
        constraint book_author_constraints REFERENCES Author (id),
    genre_id  bigint REFERENCES Genre (id),
    ISBN      INT NOT NULL
);



--changeset elnur:2
alter table book
    add column if not exists deleted bool;

--changeset elnur:3
alter table book
    alter column deleted set default false;

--changeset elnur:4
update book
set deleted = false
where deleted is null;

--changeset elnur:5
alter table book
    add column if not exists created_at timestamp;
alter table book
    add column if not exists updated_at timestamp;

alter table author
    add column if not exists created_at timestamp;
alter table author
    add column if not exists updated_at timestamp;

alter table genre
    add column if not exists created_at timestamp;
alter table genre
    add column if not exists updated_at timestamp;

--changeset elnur:6
alter table author
    add column if not exists deleted bool default false;
alter table genre
    add column if not exists deleted bool default false;

--changeset elnur:7
Insert Into Author(id, first_name, patronymic_name, last_name, birthday_date, created_at, updated_at, deleted)
VALUES (nextval('hibernate_sequence'), 'Fedor', 'Mihailovich', 'Dostoevskii', '24.03.1992', now(), now(), false);
Insert Into Genre(id, name, created_at, updated_at, deleted)
VALUES (nextval('hibernate_sequence'), 'Drama', now(), now(), false);
Insert Into Genre(id, name, created_at, updated_at, deleted)
VALUES (nextval('hibernate_sequence'), 'Roman', now(), now(), false);
